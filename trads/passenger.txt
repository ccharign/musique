Stucture:
--------
Une phrase = 4 mesure. Am F C G Am F C Em

C1: 4 phrases (3 voix, 1 guitare)
C2 : idem
LaLaLa : 2 phrases

C3 : encore 3+1
C2 : 1+2+1

LaLaLa : 2 phrases

pont + solo : 8 phrases

LaLaLa : 2 phrases

C5 : 2+1+2 (désolé c’est pas pair :( )



C1
Je suis un étranger
Et je ne fais que traverser
Traverser la ville à l'envers
Et longer la vie de travers
Derrière la brume d’un réverbère
Une étoile mouillée s'éclaire
(guitare)
(guitare)


C2
Je suis un étranger
Et je traverse à côté
Des néons des supermarchés
Sous les étoiles à peine allumées
Et la ville se désagrège
Dans un scintillant manège.
(guitare)
(guitare)


R
(guitare solo proche des lalala ?)
La la la la lalalala
La la la la lalalala
La la la la lalalala
Lalala

C3
Oh viens à mes côtés
Car nous sommes deux étrangers
Qui ne comprendront jamais
Mais nous pouvons regarder
Les étoiles qui désagrègent
Les néons des manèges.
(guitare)
(guitare)

(Mini solo possible ici)
(guitare)
(guitare)
Elle et lui dans le brouillard
Digérés le long des boulevards
Et dans leurs pas, mélangés
Le monde est un étranger
(guitare)
(guitare)



(Pont)
(guitare solo par dessus ?)
Et s’il ouvre les yeux
Que voit-il autour d’eux ?
Il voit les parkings et le béton
Il voit toutes nos créations
Archipels dans le bitume
Et une étoile dans la brume
Et tout existe pour toi et moi
Tout est créé pour toi et moi
ouvre les yeux, notre œuvre est là

(solo)

(R)

C5
Au retour dans le monde
La rue, la lumière des phares
Froids éclairs déchirent l’ombre
Éclairent et écorchent le regard
(guitare tranquille)
(guitare tranquille)
Dernière étoile efface le ciel
Sans surprise tu penses à elle
Car les étoiles sont toutes à elle
Et tout est vide vide vide sans elle
