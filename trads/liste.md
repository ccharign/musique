

- *L’étranger* (Iggy Pop / David Bowie)

  https://www.youtube.com/watch?v=-fWw7FE9tTo&ab_channel=IggyPopOfficial
  
  https://framagit.org/ccharign/musique/-/blob/master/trads/passenger.txt
  

- *Paint it gray* (Stones)
https://framagit.org/ccharign/musique/-/blob/master/trads/paint%20it,%20black.txt


- *Boot stomping on human face, forever* (Bad religion / Georges Orwell)
https://www.youtube.com/watch?v=dX1f6H78Y_k
https://framagit.org/ccharign/musique/-/blob/master/trads/boot%20stomping%20on%20human%20face.txt


- *In the shadow of the valley of death* (Marylin Manson).
Partition : https://www.songsterr.com/a/wsa/marilyn-manson-in-the-shadow-of-the-valley-of-death-tab-s22230t2
https://www.youtube.com/watch?v=YfN7eGffOto
https://framagit.org/ccharign/musique/-/blob/master/trads/in%20the%20shadow%20of%20the%20valley.txt


- *About a girl* (Nirvana / Romain Gary).
https://www.youtube.com/watch?v=AhcttcXcRYY
https://framagit.org/ccharign/musique/-/blob/master/trads/about%20a%20girl.txt


- *Le marchand de sable* (Metallica / Yves Duteil)
 https://www.youtube.com/watch?v=XZuM4zFg-60
 https://framagit.org/ccharign/musique/-/blob/master/trads/enter%20sandman.txt
 

- Seemann (Rammstein)

- la licorne rose (Wolfmother)

- boulevard Jean Jaurès (Green day)

- *Tué au nom de* (RATM / Brassens)
 https://framagit.org/ccharign/musique/-/blob/master/trads/killing%20in%20the%20name%20of
 
 
 

Textes prêts
============

 
 
 - *Temps* (Rammstein / Lamartine)
 https://www.youtube.com/watch?v=EbHGS_bVkXY
 https://framagit.org/ccharign/musique/-/blob/master/trads/zeit.txt
 
 
 - *Untill it sleeps* (Metallica)
 https://www.youtube.com/watch?v=zbKM_VAbd5o
 https://framagit.org/ccharign/musique/-/blob/master/trads/until%20it%20sleeps.txt

 
 - *Weit Weg* (Rammstein)
 https://www.youtube.com/watch?v=N9AalJuwLyQ
 https://framagit.org/ccharign/musique/-/blob/master/trads/weit%20weg.txt
