https://www.youtube.com/watch?v=N9AalJuwLyQ



C1:
L’heure est douce la lune éclaire
Les draps défaits sa peau ses yeux
La lune est douce, et l’heure est claire
Ta vie de fait, là sous tes yeux

Tu sais déjà ce qu’elle va dire
Allongée là tout contre toi
Tu entends encore les rires
Allongés là comme autrefois

R:
Ganz nah
So weit weg von hier
So nah
Weit weit weg sind wir

C2:
Sa voix est douce ses yeux sont fermes
Sa peau est chaude ses yeux t’enferment
Sa voix est chaude ses mots sont clairs
Sa peau est douce sa voix de fer

Tout contre toi elle va dormir
Tout contre toi elle va partir
Son corps est chaud ses yeux lointains
Ses yeux au chaud son cœur éteint

R

pont:
Wieder ist es Mitternacht
Sie stehlen uns das Licht die Sonne
Weil es immer dunkel ist
Wenn das Mund die Sterne küss

Tes yeux se rouvriront mouillés,
Mais ils se ferment sans t’écouter.
Peux-tu lui dire un dernier mot ?
Le dernier mot sera de trop


R en français ?

Si proche
tellement loin parfois
Si proche
Oh loin si loin, de toi
