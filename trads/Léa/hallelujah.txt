On dit qu’il est un accord secret
Qui sonne au cœur longtemps après
Que se soit éteinte sa voix
Oui c’est ainsi la quinte la quarte
Les royaumes tremblent les mers s’écartent
Dans l’air qui chante un hallelujah

Perdu un soir au fond d’une brume
Je te vis baignée d’un rayon de lune
Une larme dans ta voix
Alors le monde fut détruit
De sous ces cendres sans un bruit
D’un souffle tu fis un hallelujah

Un instant je vis la lueur
Qui brûle et t’anime à l’intérieur
Mais à présent les rideaux sont bas
Mais souviens toi quand nous volions ensemble
Le cœur en flamme l’esprit en cendres
Et chaque soupir était un hallelujah

Mon lutin parti faire un tour
Tout ce que j’ai jamais su de l’amour
C’est la pluie du retour sur chaque pas
Et pas un cri ne déchire la nuit
Pas une larme ne s’arrondit
Dans le froid se brise un hallelujah

Tout c’que j’ai pu, c’est pas grand chose
Enfermé dans nes lèvres closes
La voix éteinte chercher ton bras
Et puis en guise de dernier refrain
Quelques accords mineurs et vains
N’emportent rien, rien qu’un hallelujah
