Intro ×4

C1
Un jour on disparaîtra toi et moi
On laissera tout en plan
Un matin un chemin une besace et puis toi
On s’enfuira sans un plan
sans un plan

C2
En haut des monts ou bien au fond des bois
Avoir la vie partagée
Au creux des bras ou bien au fond d’un drap
Rien d’autre à faire qu’à s’aimer
S’aimer

R
Un jour, on disparaîtra
Un jour là, un autre pas
Voir si le cœur de la vie bat en toi
Et tu m’emmèneras
emmène moi


C3
Un jour nous tendra la vie du bout des doigts
On y jouera tu verras
De la faim et du froid, des richesses et des choix
On en jouera si t’y crois
J’y crois

C4
Les flaques de lumières dans les branches ont parfois
La saveur des sons que tu vois
Partout c’est tellement grand que vite on comprendra
Que nulle part c’est chez toi, chez moi, chez nous quoi
