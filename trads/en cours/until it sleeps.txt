Où ai-je pris ce vague à l’âme ?
Solitude, ma vieille compagne


Tu m’as ouvert déchiré
Dis moi qu’est-ce que tu as trouvé ?
Sentiment qui résonne
Oh cache le, jusqu’à ce qu’il dorme


Ou ai-je pris ce mal à l’âme ?
Dans ton regard comme une lame

Tu m’as ouvert disséqué
Dis moi qu’est-ce que tu y a caché ?
Un amour non conforme
Enfouis le, jusqu’à ce qu’il dorme

Déchire moi retiens moi
Découpe moi retiens moi
Enfouis moi retiens moi
Regarde moi garde moi garde moi
Tant qu’elle me gagne


Ma toute belle ô mon scalpel
Étincelante, sans appel
Sans appel

Voix de miel mots de fer
Un souvenir et puis t’extraire
Ma folie et mes rêves
Enfouis-les pour qu’ils s’achèvent


Déchiré oublier
Enterrer oublier

Oh t’oublier t’oublier t’oublier
inachevés



brouillons

Solitude vieille compagne
Retiens moi, tant qu’elle me gagne
