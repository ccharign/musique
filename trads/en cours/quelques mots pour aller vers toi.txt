Quelques mots pour aller vers toi (Nothing else matters/Metallica)

(C1)
Fin d’hiver dans mon café
Fin du verre, j’attends ma fée
Elle m’a promis pour cette fois
Quelques mots pour aller vers toi

(C2)
Un peu banals et maladroits
Un peu bancals mais rien qu’à toi
Trop bien peignés et trop étroits
Quelques mot à lancer vers toi

(C3)
Dans la mousse de mon noisette
Dans un repli de tes fossettes
Par hasard, tout à coup
Quelques mots trois fois tout


(R)
Mots qui dansent sur leur silences
Mots qui pansent nos différences
Quelques mots pour aller vers toi


(C4)
Pour te chercher et peu à peu
Voyager derrière tes yeux
Vagabonder avec ta voix
Quelques pas juste avec toi

(C5)
Une danse et un sourire
Un silence et un soupir
Perds le sens de tes pensées
Quelques pas les yeux fermés


(R)

(solo)

(C6)
Faim de vers, soif de prose
À la fin d’un verre est-ce que j’ose
Quelques phrases toutes à l’envers
Quelques mots pour aller vers

(outro)



Brouillons
----------

Râcler la mousse de mon noisette
Aussi loin dans mon café
Je m’accroche
assemblés en doubles-croches

Quelques mots, trois fois tout

Quelques rimes sur un abyme

Mots qui passent et qui s’enlacent

Passer le pont de ton regard
Vagabonder dans tes hasards

