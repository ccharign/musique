                      Am               Em
              Comme le vent, balaie la route
                      Am                Em
              et la pluie, hésite et doute
                      Am                  Em
              entre les gouttes, je les vois toutes
                     Am
mes erreurs et mes fausses routes


 
et je rêve encore de toi
et le vent sait tous mes regrets
et je rêve encore à toi
mais rien ne peut plus l'empêcher
et je  sais
  ce que t'as coûté de t'en aller


et je rève, perdu dans les ombres du soir
sur un chemin qui ne mène nulle part
il fait froid, il pleut sur le monde ce soir
et toi, tu n'es plus nulle part
       et c'est l'heure du départ


   le vent, cache ma voix
et la pluie, pleure parfois
on voit les arbres, mirages
où tu m'embrassais sous l'orage

et je pense encore à toi
Chaque souvenir pique où ça fait mal
et je pense encore à toi
et le vent sait que je t'ai fait mal
mais s'enfuient
 les rêves les cris et nos folies
 
et je vais perdu dans les ombres noires
et la vie ne mène nulle part
je te crois, il pleut sur nos coeurs ce soir
et toi, je te vois de toute part
       et c'est l'heure du départ

(pont)

     j'ai besoin  de toi
   et ni le vent, et ni la pluie
   ne me détourne de toi
et tous mes rêves, toute ma folie
      sont restés  à toi
oh prend ce coeur ces miettes ce froid,
   c'est moi



surpris, par  l'aube et ses ses lueurs
    la nuit, ne cache plus les pleurs
    
  une    larme, je crois encore déborde
mais les armes, rendues je vire de bord   
   retourne, et cadenasse ton coeur
car le monde, ne dois pas voir la douleur
             n'accepte pas l'erreur


    et je vais ranger tous mes doutes au fond
car c'est l'heure, de rentrer à la raison
                   de la fin de la passion
                   du néant, de la raison
