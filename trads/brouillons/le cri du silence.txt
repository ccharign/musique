AM                      G
   solitude ma vieille amie
                       Am
nous sommes encore réunis
                     F      C
J'ai cru te fuir,  m'étourdir
                     F       C
je crie je lutte, je prie je doute,
       F                                        C
mes gesticulations creusent le vide autour de moi
              Am
j'entends ta voix
              G          Am
    comme le cri du silence


j'écris de sous l'érable
où tu étais belle en robe bleue
les colchique fêtent l'automne
ton sourire encore résonne
de ce printemps déjà si loin, déjà éteint
 ne revient
   que le cri, du silence


ces matins sans horizon
et ces journées sans raison
à t'appeler, te désirer
te rechercher   te réver
et tout empli de ton absence j'attends
 j'entends
  le cri du silence

mais les mots s'évanouissent
et les rèves dans la nuit glisse
les cordes de cette guitare s'envolent
avec sa voix,  sans paroles
et vous mes mots jamais écrits vous saurez
la trouver
lui parler
 au coeur du silence
 
 
 
